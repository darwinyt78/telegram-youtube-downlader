const TelegramBot = require("node-telegram-bot-api");
const ytdl = require("ytdl-core");
const { google } = require("googleapis");
const fs = require("fs");
require("dotenv").config();

// Доступ до змінних оточення
const API_KEY = process.env.API_KEY;
const BOT_TOKEN = process.env.BOT_TOKEN;

// Установка токена для Telegram бота
const bot = new TelegramBot(BOT_TOKEN, { polling: true });

// Оброблювач для команди Оброблювач для команди /start
bot.onText(/\/start/, (msg) => {
  const chatId = msg.chat.id;
  const messageText =
    "Вітаю! Надішліть мені посилання на відео з YouTube, і я скачаю його тобі.";

  const keyboard = {
    reply_markup: {
      keyboard: [[{ text: "Завантажити відео" }]], // масив масивів кнопок, тут використовується лише одна кнопка "Завантажити відео"
      resize_keyboard: true, // параметр, який дозволяє змінювати розмір клавіатури
      one_time_keyboard: true, // параметр, який приховує клавіатуру після натискання кнопки
    },
  };

  // Надсилання повідомлення з клавіатурою
  bot.sendMessage(chatId, messageText, keyboard);
});

// Обробник для текстових повідомлень та натискання на кнопку
bot.on("message", async (msg) => {
  const chatId = msg.chat.id;
  const messageText = msg.text;

  if (messageText === "Завантажити відео") {
    // Відповідь на натискання кнопки
    bot.sendMessage(chatId, "Чудово давай почнемо!");
    bot.sendMessage(chatId, "Кидай посилання на відео, яке хочеш завантажити");
    downloadVideo();
  }
});

function downloadVideo() {
  bot.onText(
    /https:\/\/www\.youtube\.com\/watch\?v=(.+)/,
    async (msg, match) => {
      const chatId = msg.chat.id;
      const videoUrl = match[0];

      // Вилучення ідентифікатора відео з посилання
      const parsed = new URL(videoUrl);
      const videoId = parsed.searchParams.get("v");

      // Створення YouTube Data API клієнта
      const youtube = google.youtube({
        version: "v3",
        auth: API_KEY,
      });

      try {
        // Завантаження відео за допомогою ytdl-core
        const videoInfo = await ytdl.getInfo(videoUrl);

        // Запит на отримання інформації про відео
        const response = await youtube.videos.list({
          part: "snippet,statistics",
          id: videoId,
        });

        // Обробка відповіді та отримання необхідної інформації
        const video = response.data.items[0];
        const title = video.snippet.title;
        const author = video.snippet.channelTitle;
        const description = video.snippet.description;
        const viewCount = video.statistics.viewCount;

        // Завантаження відео
        const videoFormat = ytdl.chooseFormat(videoInfo.formats, {
          quality: "highest",
        });
        const videoStream = ytdl.downloadFromInfo(videoInfo, {
          format: videoFormat,
        });

        videoStream.pipe(fs.createWriteStream(`${title}.mp4`));

        bot.sendMessage(chatId, "Відео завантажується");

        // Відправлення відео файлом користувачеві
        setTimeout(() => {
          bot.sendDocument(chatId, fs.createReadStream(`${title}.mp4`));
          const message = `Відео завантаженно. \n Назва: ${title} \n Автор: ${author} \n Кількість переглядів: ${viewCount}`;
          bot.sendMessage(chatId, message);

          // Видалення завантаженого файлу

          setTimeout(
            () =>
              fs.unlink(`${title}.mp4`, (err) => {
                if (err) {
                  console.error(err);
                  return;
                }
                console.log(`${title}.mp4 було видалено`);
              }),
            4000
          );
        }, 5000);
      } catch (error) {
        console.error(error);

        // Надсилання повідомлення про помилку, якщо щось пішло не так
        bot.sendMessage(
          chatId,
          "Виникла помилка. Перевірте посилання на відео та спробуйте знову."
        );
      }
    }
  );
}

// Запуск бота
bot.on("polling_error", (error) => {
  console.error(error);
});
